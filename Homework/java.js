var gen_1 = {
    nome : "Rupert" , 
    cognome : "Sciamenna",
    cod_fis : "AGD46E7D"
} ;

var gen_2 = {
    nome : "Maccio" , 
    cognome : "Capatonda",
    cod_fis : "SGSG6DJ"
} ;
var gen_3 = {
    nome : "Ivo" , 
    cognome : "Avido",
    cod_fis : "S63TDNS9"
} ;

var fig_1 = {
    nome : "Herbert" , 
    cognome : "Ballerina",
    cod_fis : "AG5473GD"
} ;
var fig_2 = {
    nome : "Fabbio" , 
    cognome : "DiNinno",
    cod_fis : "AGD46E54D"
} ;
var fig_3 = {
    nome : "Riccardino" , 
    cognome : "Fuffolo",
    cod_fis : "ADNJE7D"
} ;

var elenco_figli = [];
var elenco_genitori = [];

elenco_figli.push(fig_1);
elenco_figli.push(fig_2);
elenco_figli.push(fig_3);

elenco_genitori.push(gen_1);
elenco_genitori.push(gen_2);
elenco_genitori.push(gen_3);

function elimina(){
    debugger;
    let elenco_figli_string = localStorage.getItem("elenco_figli");
    let elenco_figli_json = JSON.parse(elenco_figli_string);

    
        for(let i=0;i<elenco_figli_json.length;i++){
            debugger;
            if ($("#check-figli").prop("checked")){
            elenco_figli_json.splice(i,1);
            }
        }
    localStorage.setItem("elenco_figli",JSON.stringify(elenco_figli_json));
}

function eliminaGenitore(){
    debugger;
    let elenco_genitore_string = localStorage.getItem("elenco_genitori");
    let elenco_genitore_json = JSON.parse(elenco_genitore_string);

    
        for(let i=0;i<elenco_genitore_json.length;i++){
            if ($("#check-genitori").is(":checked")){
            elenco_genitore_json.splice(i,1);
            }
        }
    localStorage.setItem("elenco_genitori",JSON.stringify(elenco_genitore_json));
}



$(document).ready( 
    function(){

        // localStorage.setItem("elenco_genitori", JSON.stringify(elenco_genitori));
        // localStorage.setItem("elenco_figli", JSON.stringify(elenco_figli));

        
        

        $("#btn-stampa-genitori").on("click",
        function(){
            $("#Stampa-lista-genitori").empty();
            let elenco_genitori_string = localStorage.getItem("elenco_genitori");
            let elenco_genitori_json = JSON.parse(elenco_genitori_string);
            stampaTabella(elenco_genitori_json);
        } ) ;

        $("#btn-stampa-figli").on("click",
        function(){
            debugger;
            $("#Stampa-lista-figli").empty();
            let elenco_figli_string = localStorage.getItem("elenco_figli");
            let elenco_figli_json = JSON.parse(elenco_figli_string);
            stampaTabellaFigli(elenco_figli_json);
        }           
        );

        $("#btn-elimina-figli").on("click",
            function(){
                elimina();
                let elenco_figli_string = localStorage.getItem("elenco_figli");
                let elenco_figli_json = JSON.parse(elenco_figli_string);
                $("#Stampa-lista-figli").empty();
                stampaTabellaFigli(elenco_figli_json);
            }
        )
         
        $("#btn-elimina-genitori").on("click",
        function(){
            eliminaGenitore();
            let elenco_genitori_string = localStorage.getItem("elenco_genitori");
            let elenco_genitori_json = JSON.parse(elenco_genitori_string);
            $("#Stampa-lista-genitori").empty();
            stampaTabella(elenco_genitori_json);
        }
    )

        $("#btn-inserisci").on("click",
            function(){
                let var_nome = $("#input-nome").val();
                let var_cognome = $("#input-cognome").val();
                let var_data = $("#input-data").val();
                let var_cod = $("#input-codfis").val();
                debugger;
                let var_gf = $("#input-ruolo").val();

                let new_ins = {
                    nome : var_nome, 
                    cognome : var_cognome,
                    cod_fis : var_cod,
                    data: var_data
                } ;

                debugger;
                switch (var_gf) {
                    case "G":
                        elenco_genitori.push(new_ins);
                        localStorage.setItem("elenco_genitori", JSON.stringify(elenco_genitori));
                        alert("Inserimento avvenuto con successo");
                        break;

                    case "F":
                        elenco_figli.push(new_ins);
                        localStorage.setItem("elenco_figli", JSON.stringify(elenco_figli));
                        alert("Inserimento avvenuto con successo");

                        break;
                
                    default:
                        alert("Imposta se figlio o genitore!");
                        break;
                }
            }
        )




        function stampaTabella(elenco_genitori){
            let string_check =  "<div class=\"form-check\">"+
                                    "<input class=\"form-check-input\" type=\"checkbox\" value=\"\" id=\"check-genitori\">"+
                                    "<label class=\"form-check-label\" for=\"check-genitori\">"+
                                    ""+
                                   " </label>"+
                                "</div>" ;

          
            for (let i = 0; i < elenco_genitori.length; i++) {
                let string_tabella ="<tr>"+
                                "<td>"+elenco_genitori[i].nome+"</td>"+
                                "<td>"+elenco_genitori[i].cognome+"</td>"+
                                "<td>"+elenco_genitori[i].cod_fis+"</td>"+
                                "<td>"+string_check+"</td>"+
                                "</tr>" ; 
                $("#Stampa-lista-genitori").append(string_tabella);     
            }
        }

        function stampaTabellaFigli(elenco_figli){
            let string_check =  "<div class=\"form-check\">"+
                                    "<input class=\"form-check-input\" type=\"checkbox\" value=\"\" id=\"check-figli\">"+
                                    "<label class=\"form-check-label\" for=\"check-figli\">"+
                                    ""+
                                    " </label>"+
                                "</div>" ;
                for (let i = 0; i < elenco_figli.length; i++) {
                let string_tabella ="<tr>"+
                                "<td>"+elenco_figli[i].nome+"</td>"+
                                "<td>"+elenco_figli[i].cognome+"</td>"+
                                "<td>"+elenco_figli[i].cod_fis+"</td>"+
                                "<td>"+string_check+"</td>"+
                                "</tr>" ; 
                $("#Stampa-lista-figli").append(string_tabella);     
            
        }
        
    }
}
);







