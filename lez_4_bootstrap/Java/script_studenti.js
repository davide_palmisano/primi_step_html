


$(document).ready(
    function () {

        var array_studenti = [] ;
        
        $("#ins-studente").on("click",
            function () {
                let nome = $("#input_nome").val(); 
                let cognome = $("#input_cognome").val() ; 
                let matricola = $("#input_matricola").val() ; 
                let sesso = $("#select_sesso").val() ; 

                let new_student = {
                    nom: nome,
                    cogn: cognome,
                    mat: matricola,
                    ses: sesso

                }

                let presente = ricercaMatricola(new_student.mat,array_studenti) ; 

                if(presente){
                    alert("Studente già presente!");
                }
                else{
                console.table(new_student);
                array_studenti.push(new_student) ;
                alert("Studente inserisci con successo!");
                $("#tabella-output").empty();
                stampaTabella(array_studenti);
                svuotaCampo();
                }
                }  
        );   
    }
);

function stampaTabella(array_studenti) {

    let tabella_string = "";
    for(let i=0; i<array_studenti.length;i++){

        tabella_string += "<tr>"+"<td>" +array_studenti[i].nom + "</td>"
                                +"<td>" +array_studenti[i].cogn + "</td>"
                                +"<td>" +array_studenti[i].mat + "</td>"
                                +"<td>" +array_studenti[i].ses + "</td>"
                         "</tr>"
    }

    $("#tabella-output").append(tabella_string);
}

function ricercaMatricola(var_matricola, array_studenti) {
    let presente = false;

    for(let i=0; i<array_studenti.length;i++){

        if (array_studenti[i].mat == var_matricola){
            presente = true ; 
        }
    }

    return presente ; 
}

function svuotaCampo() {
    $("#input_nome").val("") ;
    $("#input_cognome").val("") ; 
    $("#input_matricola").val("") ; 
    $("#select_sesso").val("M") ; 
}