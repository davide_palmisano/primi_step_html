var array_iscritti = [] ;

$(document).ready(
    function() {

        
        $("#container-output").hide() ;

        $("#svuota").on("click",
           function () {
                array_iscritti.empty() ;
                $("#container-output").hide() ;
                $("#tabella-output").hide() ;
                $("#tabella-output").empty() ;  
            }
        );

        $("#iscriviti").on("click",
            function () {
                 
                
                let nome = $("#input-nome").val();
                let cognome = $("#input-cognome").val();
                let data = $("#input-data").val();

                let new_iscritto = {
                    nom: nome,
                    cogn: cognome,
                    dat: data
                }

                if(verifica18(data)){
                    $("#container-output").show() ;
                    $("#tabella-output").show() ;
                    array_iscritti.push(new_iscritto) ;
                    $("#tabella-output").empty() ;
                    stampaTabella() ;
                    alert("Inserimento avvenuto!")                   
                }

                else {
                    alert("Persona minorenne!")
                }
            }
            );
    }
);

function stampaTabella(){
    // let stringa_tabella = "";
    for (let i = 0; i < array_iscritti.length; i++) {
   
         let stringa_tabella =   "<tr>" +
                                    "<td>" + array_iscritti[i].nom + "</td>" +
                                    "<td>" + array_iscritti[i].cogn+ "</td>" +
                                    "<td>" + array_iscritti[i].dat + "</td>" +
                                "</tr>";
        $("#tabella-output").append(stringa_tabella) ;       
    }

}

function verifica18(var_data){
    let oggi = new Date();
    let maggiorenne_lim = oggi.getFullYear() - 18 ;
    let oggi_diciotto = new Date();
    oggi_diciotto.setFullYear(maggiorenne_lim);
    let anni = Date.parse(oggi_diciotto)-Date.parse(var_data); 
    maggiorenne = false ;
    if(anni>0){
        maggiorenne = true;
    }
    return maggiorenne ; 
}

